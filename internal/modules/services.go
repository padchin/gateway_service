package modules

import (
	aservice "gitlab.com/padchin/gateway_service/internal/modules/auth/service"
	uservice "gitlab.com/padchin/gateway_service/internal/modules/user/service"
	"net/rpc"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServices(rpcClientUser *rpc.Client, rpcClientAuth *rpc.Client) *Services {
	return &Services{
		User: uservice.NewUserServiceJSONRPC(rpcClientUser),
		Auth: aservice.NewAuthServiceJSONRPC(rpcClientAuth),
	}
}
