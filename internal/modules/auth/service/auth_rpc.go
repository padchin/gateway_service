package service

import (
	"context"
	"net/rpc"
)

type AuthServiceJSONRPC struct {
	client *rpc.Client
}

func NewAuthServiceJSONRPC(client *rpc.Client) *AuthServiceJSONRPC {
	return &AuthServiceJSONRPC{client: client}
}

func (c *AuthServiceJSONRPC) Register(ctx context.Context, in RegisterIn) RegisterOut {
	var out RegisterOut
	err := c.client.Call("AuthRPCService.Register", in, &out)
	if err != nil {
		return RegisterOut{}
	}
	return out
}

func (c *AuthServiceJSONRPC) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	var out AuthorizeOut
	err := c.client.Call("AuthRPCService.AuthorizeEmail", in, &out)
	if err != nil {
		return AuthorizeOut{}
	}
	return out
}

func (c *AuthServiceJSONRPC) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	var out AuthorizeOut
	err := c.client.Call("AuthRPCService.AuthorizeRefresh", in, &out)
	if err != nil {
		return AuthorizeOut{}
	}
	return out
}

func (c *AuthServiceJSONRPC) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	var out VerifyEmailOut
	err := c.client.Call("AuthRPCService.VerifyEmail", in, &out)
	if err != nil {
		return VerifyEmailOut{}
	}
	return out
}
