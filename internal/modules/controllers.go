package modules

import (
	"gitlab.com/padchin/gateway_service/internal/infrastructure/component"
	acontroller "gitlab.com/padchin/gateway_service/internal/modules/auth/controller"
	ucontroller "gitlab.com/padchin/gateway_service/internal/modules/user/controller"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontroller.Userer
}

func NewControllers(components *component.Components) *Controllers {
	authController := acontroller.NewAuth(components)
	userController := ucontroller.NewUser(components)

	return &Controllers{
		Auth: authController,
		User: userController,
	}
}
