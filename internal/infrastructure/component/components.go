package component

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/padchin/gateway_service/config"
	"gitlab.com/padchin/gateway_service/internal/infrastructure/responder"
	"gitlab.com/padchin/gateway_service/internal/infrastructure/tools/cryptography"
	"gitlab.com/padchin/gateway_service/internal/modules"
	"go.uber.org/zap"
)

type Components struct {
	Conf         config.AppConf
	TokenManager cryptography.TokenManager
	Responder    responder.Responder
	Decoder      godecoder.Decoder
	Logger       *zap.Logger
	Hash         cryptography.Hasher
	Services     *modules.Services
}

func NewComponents(
	conf config.AppConf,
	tokenManager cryptography.TokenManager,
	responder responder.Responder,
	decoder godecoder.Decoder,
	hash cryptography.Hasher,
	logger *zap.Logger,
	services *modules.Services,
) *Components {
	return &Components{
		Conf:         conf,
		TokenManager: tokenManager,
		Responder:    responder,
		Decoder:      decoder,
		Hash:         hash,
		Logger:       logger,
		Services:     services,
	}
}
